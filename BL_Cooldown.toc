## Interface: 60000
## Version: 3.55
## Title: |cffc41f3bBlood Legion|r Cooldown
## Notes: Minimalistic Visual Raid Cooldowns
## OptionalDeps: ElvUI, LibStub, CallbackHandler-1.0, AceEvent-3.0, AceTimer-3.0
## SavedVariables: BLCDDB
## SavedVariablesPerCharacter: BLCDrosterReload
## DefaultState: Enabled
## LoadOnDemand: 0

libs\load_libs.xml
load_bl_cooldown.xml